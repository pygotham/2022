---
title: Registration Information
---

{% if site.data.event.registration_closed %}
  Registration for {{ site.data.event.name }} is no longer open.
{% elsif site.data.event.registration_url %}
  To register for {{ site.data.event.name }}, visit the
  [registration page]({{ site.data.event.registration_url }}).
{% else %}
  Registration for {{ site.data.event.name }} isn't open yet.
{% endif %}

## Prices
{{ site.data.event.name }} is free to attend. See you there!
