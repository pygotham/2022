---
layout: post
title: Postponing the PyGotham Film Fest Until 2023
date: 2022-10-30 14:07 -0400
---
As we've [previously announced], the PyGotham Film Festival will not take place
as originally scheduled. We now plan to premiere film submissions as part of
PyGotham's 2023 event. We'll have more details to share about PyGotham 2023 in
the coming months. We thank the entire PyGotham community for your patience and
continued support throughout the years, and we look forward to meeting again in
2023.

[previously announced]: {% post_url 2022-07-16-the-pygotham-film-festival-is-postponed %}
