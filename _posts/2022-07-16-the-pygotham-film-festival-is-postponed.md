---
layout: post
title: The PyGotham Film Festival is Postponed
date: 2022-07-16 13:21 -0400
---
The PyGotham Film Festival will not take place October 7 - 8 as originally
scheduled. We'll post an update here when we have new dates to share. In the
mean time, we've reopened our call for proposals; if you have a film idea you'd
like to present at the festival, [propose it
here](https://pretalx.com/the-pygotham-film-festival-2022/cfp).

We recognize that this is a new type of conference for most of our community.
Producing a short film relevant to the Python community is a lot to ask, and we
want to make sure we can support our filmmakers with all of the time and
production assistance they need in order to realize their vision. We look
forward to working with the filmmakers in our community and seeing their final
products. If you have an idea for a proposal but are on the fence or want to
discuss your film concept with the organizers before submitting it, feel free to
reach out to [organizers@pygotham.org](mailto:organizers@pygotham.org).

Thank you to everyone who's been supportive of this conference so far. Stay
tuned to this website and our [Twitter account](https://twitter.com/PyGotham)
for upcoming announcements regarding festival dates and featured films.

\- The PyGotham Organizers
